package init.moviemanagement.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {

    @Autowired
    private  CategoryRepository categoryRepository;

    public List<Category> getCategories() {
        List<Category> categories = new ArrayList<>();
        categoryRepository.findAll().forEach(categories::add);
        return categories;
    }
    public Optional<Category> getCategory(String id){
        return categoryRepository.findById(id);
    }
    public void addCategory(Category category) {

        categoryRepository.save(category);
    }

    public void  updateCategory(Category category, String id){
        //remove string id parameter
        categoryRepository.save(category);
        return;
    }

    public void deleteCategory(String id) {
        categoryRepository.deleteById(id);
    }

}

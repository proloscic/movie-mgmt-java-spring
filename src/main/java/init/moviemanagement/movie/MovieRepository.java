package init.moviemanagement.movie;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MovieRepository extends JpaRepository<Movie, String> {

    //Search movie by name
    @Query("SELECT m FROM Movie m WHERE m.name LIKE %?1%")
    public List<Movie> findByName(String keyword);
    
    @Query("SELECT m FROM Movie m WHERE m.id LIKE %?1%")
    public Page<Movie> findByMovieId(String id, Pageable pageable);
}

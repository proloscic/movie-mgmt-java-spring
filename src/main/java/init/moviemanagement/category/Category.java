package init.moviemanagement.category;

import init.moviemanagement.movie.Movie;
import jakarta.persistence.*;

import java.util.Set;

@Entity
public class Category {
    @Id
    private String id;
    private String name;

    @ManyToMany()
    @JoinTable(name = "movie_category", joinColumns = { @JoinColumn(name = "category_id") },
            inverseJoinColumns = { @JoinColumn(name = "movie_id") })
    private Set<Movie> movies;

    public Category() {

    }
    public Category(String id, String name) {
        this.id = id;
        this.name = name;
    }
    public Category(String id, String name, Set<Movie> movies) {
        this.id = id;
        this.name = name;
        this.movies = movies;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies){
        this.movies = movies;
    }

}

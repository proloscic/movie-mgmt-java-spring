package init.moviemanagement;

import init.moviemanagement.category.Category;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import com.google.gson.Gson;

import init.moviemanagement.movie.Movie;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class MovieManagementApplicationTests {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private Gson gson;

	@WithMockUser(roles="ADMIN")
	@Test
	public void adminPOSTRequestOnCategories_shouldSucceedWith200() throws Exception {
		Category category = new Category("sf","sf movies");
		Gson gson = new Gson();
		mvc.perform(post("/categories").contentType(MediaType.APPLICATION_JSON).content(gson.toJson(category)))
				.andExpect(status().isOk());
	}

	@WithMockUser(roles="USER")
	@Test
	public void userPOSTRequestOnCategcoriesRequest_ShouldFail403() throws Exception {
		Category category = new Category();
		Gson gson = new Gson();
		mvc.perform(post("/categories").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isForbidden());
	}
	@WithMockUser(roles="USER")
	@Test
	public void userGETRequestOnCategories_shouldSucceedWith200() throws Exception {
		mvc.perform(get("/categories").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	@WithMockUser(roles="USER")
	@Test
	public void userPOSTRequestOnMovies_shouldSucceedWith200() throws Exception {
		Movie movie = new Movie("scarface","Scarface");
		Gson gson = new Gson();
		mvc.perform(post("/movies").contentType(MediaType.APPLICATION_JSON).content(gson.toJson(movie)))
				.andExpect(status().isOk());
	}
	
	@WithMockUser(roles="")
	@Test
	public void NoRolePOSTRequestOnMovies_shouldFailWith403() throws Exception {
		Movie movie = new Movie("scarface","Scarface");
		Gson gson = new Gson();
		mvc.perform(post("/movies").contentType(MediaType.APPLICATION_JSON).content(gson.toJson(movie)))
				.andExpect(status().isForbidden());
	}


}

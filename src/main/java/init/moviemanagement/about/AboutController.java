package init.moviemanagement.about;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AboutController {

    @RequestMapping("/")
    public String mainPage() {
        return "Welcome to Movie Management app!" + "\n" +
                "Available operations:" + "\n" +
                "Categories: GET|POST /categories, GET|PUT /categories/{id} "+ "\n" +
                "Movies:GET|POST /movies, GET|PUT|DELETE /movies/{id} " + "\n" +
                "- Movies pagination example GET http://localhost:8080/movies?page=1&size=2" + "\n" +
                "Binding category to existing movie:/movies/{movieId} use POST with body:categoryId" + "\n" +
                "Search movies: GET /search/{keyword}"
                ;
    }
    @RequestMapping("/about")
    public String aboutPage() {
        return "This is Movie Management app";
    }

}


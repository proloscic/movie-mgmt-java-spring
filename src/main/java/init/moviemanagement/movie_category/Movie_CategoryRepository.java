package init.moviemanagement.movie_category;

import org.springframework.data.repository.CrudRepository;
import java.util.Set;

public interface Movie_CategoryRepository extends CrudRepository<Movie_Category, String> {
    public Set<Movie_Category> findBymovieId(String name);
    public void deleteById(Integer id);
}
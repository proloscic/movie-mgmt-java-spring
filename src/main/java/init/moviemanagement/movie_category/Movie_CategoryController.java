package init.moviemanagement.movie_category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class Movie_CategoryController {
    @Autowired
    private Movie_CategoryService Movie_CategoryService;
    //bind category to movie
    @RequestMapping(method = RequestMethod.POST, value = "/movies/{id}")
    public void addMovie_Category(@RequestBody String category_id, @PathVariable String id) {
        Movie_CategoryService.addMovie_Category(id, category_id);
    }

}


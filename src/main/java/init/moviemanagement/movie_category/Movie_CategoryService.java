package init.moviemanagement.movie_category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class Movie_CategoryService {

    @Autowired
    private  Movie_CategoryRepository movie_CategoryRepository;

    public void addMovie_Category(String movieId, String category_id) {
        Movie_Category movie_category = new Movie_Category(movieId, category_id);
        movie_CategoryRepository.save(movie_category);

    }

    public List<Movie_Category> getmovie_category() {
        List<Movie_Category> movie_category = new ArrayList<>();
        movie_CategoryRepository.findAll().forEach(movie_category::add);
        return movie_category;
    }
    public Optional<Movie_Category> getMovie_Category(String id){
        return movie_CategoryRepository.findById(id);
    }


    public void  updateMovie_Category(Movie_Category movie_Category, String id){
        //remove string id parameter
        movie_CategoryRepository.save(movie_Category);
        return;
    }

    public void deleteMovie_Category(String id) {
        movie_CategoryRepository.deleteById(id);
    }

}


package init.moviemanagement.movie;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class MovieController {
    @Autowired
    private MovieService movieService;
    /*
    @RequestMapping("/movies")
    public List<Movie> getMovies(){
        return movieService.getMovies();
    }
    */
    @RequestMapping(method = RequestMethod.GET, value = "/movies")
    public Page<Movie> getMovies(Pageable pageable, @RequestParam(required =  false) String id){
        if (id == null){
        return movieService.getMoviesWithPagination(pageable);
        }
        else{
            return movieService.getMoviesByIdPagination(id, pageable);
        }
    }

    @RequestMapping("/movies/{id}")
    public Optional<Movie> getMovie(@PathVariable String id){
        return movieService.getMovie(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/movies")
    public void addMovie(@RequestBody Movie movie) {
        movieService.addMovie(movie);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/movies/{id}")
    public void updateMovie(@RequestBody Movie movie, @PathVariable String id){
        movieService.updateMovie(movie, id);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/movies/{id}")
    public void deleteMovie(@PathVariable String id) {
        movieService.deleteMovie(id);
    }

    //search movie name by keyword
    @RequestMapping("/search/{keyword}")
    public List<Movie> getMoviesByName(@PathVariable String keyword){
        return movieService.findbyName(keyword);
    }
    //search main page - show all

    @RequestMapping("/search")
    public List<Movie> searchShowAll(){
        return movieService.getMovies();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/search")
    public Page<Movie> searchShowAll(Pageable pageable){
        return movieService.getMoviesWithPagination(pageable);
    }

}

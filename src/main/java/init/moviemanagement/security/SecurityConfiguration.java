package init.moviemanagement.security;

import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {


    @Bean
        public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
            http
                    .csrf().disable()
                    .cors().and()
                    .authorizeHttpRequests()
                                    //movies: ADMIN: all; USER:POST|PUT|GET; unauthUser:GET
                                    .requestMatchers(HttpMethod.GET,"/movies/**").permitAll()
                                    .requestMatchers(HttpMethod.POST,"/movies/**").hasAnyRole("ADMIN", "USER")
                                    .requestMatchers(HttpMethod.PUT,"/movies/**").hasAnyRole("ADMIN", "USER")

                                    .requestMatchers(HttpMethod.POST, "/**").hasRole("ADMIN")
                                    .requestMatchers(HttpMethod.DELETE, "/**").hasRole("ADMIN")
                                    .requestMatchers(HttpMethod.PUT, "/**").hasRole("ADMIN")
                                    .requestMatchers(HttpMethod.GET,"/**").hasAnyRole("ADMIN", "USER")
                                    .and().httpBasic(Customizer.withDefaults());

                                    
            return http.build();
    }
    
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
    @Bean
    UserDetailsManager users(DataSource dataSource) {
        UserDetails user = User.builder()
                .username("regular")
                .password(passwordEncoder().encode("regularpass"))
                .roles("USER")
                .build();
        UserDetails admin = User.builder()
                .username("admin")
                .password(passwordEncoder().encode("admin"))
                .roles("ADMIN", "USER")
                .build();
        JdbcUserDetailsManager users = new JdbcUserDetailsManager(dataSource);
        if (!users.userExists(user.getUsername())) {
            users.createUser(user);
        }
        if (!users.userExists(admin.getUsername())) {
            users.createUser(admin);
        }
        return users;
    }
}

 
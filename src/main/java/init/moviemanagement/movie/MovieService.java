package init.moviemanagement.movie;

import init.moviemanagement.movie_category.Movie_Category;
import init.moviemanagement.movie_category.Movie_CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
@Transactional
@Service
public class MovieService {

    @Autowired
    private  MovieRepository movieRepository;
    @Autowired
    private  Movie_CategoryRepository movie_categoryRepository;
    public List<Movie> getMovies() {
        List<Movie> movies = new ArrayList<>();
        movieRepository.findAll().forEach(movies::add);
        return movies;
    }

    public Page<Movie> getMoviesWithPagination(Pageable pageable) {

        Page<Movie> movies = movieRepository.findAll(pageable);;
        return movies;
    }

    public Page<Movie> getMoviesByIdPagination(String id, Pageable pageable){
        Page<Movie> movies = movieRepository.findByMovieId(id, pageable);;
        return movies;
    }
    public Optional<Movie> getMovie(String id){
        return movieRepository.findById(id);
    }
    public void addMovie(Movie movie) {

        movieRepository.save(movie);
    }

    public void  updateMovie(Movie movie, String id){
        //remove string id parameter
        movieRepository.save(movie);
        return;
    }

    public void deleteMovie(String id) {
        Set<Movie_Category> movie_Categories = movie_categoryRepository.findBymovieId(id);
        //delete all related movie_categories
        for (Movie_Category movie_category : movie_Categories)
        {
            Integer movie_category_id = movie_category.getId();
            movie_categoryRepository.deleteById(movie_category_id);
        };
        movieRepository.deleteById(id);
    }

    //search movie name by keyword
    public List<Movie> findbyName (String keyword){
        return movieRepository.findByName(keyword);
    }

}

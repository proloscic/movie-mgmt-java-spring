package init.moviemanagement.movie_category;

import jakarta.persistence.*;

@Entity
public class Movie_Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "movie_id")
    private String movieId;
    private String category_id;

    public Movie_Category() {
    }
    public Movie_Category(String movieId, String category_id){
        this.movieId = movieId;
        this.category_id = category_id;
    }
    public Movie_Category(Integer id, String movieId, String category_id) {
        this.id = id;

        this.movieId = movieId;
        this.category_id = category_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return movieId;
    }

    public void setName(String movieId) {
        this.movieId = movieId;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }
}
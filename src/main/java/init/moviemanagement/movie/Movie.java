package init.moviemanagement.movie;

import com.fasterxml.jackson.annotation.JsonIgnore;
import init.moviemanagement.category.Category;
import jakarta.persistence.*;

import java.util.Set;

@Entity
public class Movie {
    @Id
    private String id;
    private String name;
    //---------------------------
    @JsonIgnore
    @ManyToMany
    private Set<Category> categories;

    //----------------------
    public Movie() {
    }
    public Movie(String id, String name, Set<Category> categories) {
        this.id = id;
        this.name = name;
        this.categories = categories;
    }
    public  Movie(String id, String name){
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories){
        this.categories = categories;
    }


}
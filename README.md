# movies-mgmt-spring


## Movie Management Spring Boot Java app

## Description

This is Movie Management Spring Boot Java app connected with mySQL database.

Available operations:
- All Categories: GET /categories <br>
- specific category: POST /categories; GET|PUT /categories/{id} <br>
- All Movies: GET /movies
- specific movie: POST /movies; GET|PUT|DELETE /movies/{id} 
- Search movies: By name -> GET /search/{keyword} or search by part of id -> http://localhost:8080/movies?id={id}
- Movies pagination example GET http://localhost:8080/movies?page=0&size=2
- Binding category to existing movie: /movies/{movieId} use POST with body:{categoryId}
- Category POST body example:
    {
        "id": "action",
        "name": "Action movies"
    }
- Movie POST body example: {
            "id": "diehard",
            "name": "Die hard"
        }
- Binding category to existing movie POST body example: action
- for additional binding do another POST with other categoryId

### Steps:

1. Restore database from src\main\db_movieBackup.sql
- or create two Default User Schema tables manually using defaultUserSchemaTableCreate.sql. Other tables will be created automatically with the first application run. Admin and regular users will be also added with the first run.
2. Modify aplication.properties from src\main\resources to suit your sql server and database. 
3. Run MovieManagementApplication.java src\main\java\init\moviemanagement
4. Use Postman or similar for interaction with app.

### Notes:

For Basic Auth i've added two users: 
- user:admin; pass:admin; roles:ADMIN, USER
- user:regular; pass:regularpass; 
For unauthenticated user use "No Auth" option.

by roles:
- Admin: can access all endpoints
- Regular user: can GET/POST/PUT movies and GET categories
- Unauthenticated: can only GET movies


